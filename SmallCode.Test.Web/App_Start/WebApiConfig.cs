﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SmallCode.Test.Web.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            ///web api 路由 在webapi 中的System.Web.Http 中 这是WebApi对原来的System.Web.Http的扩展
            config.MapHttpAttributeRoutes();

            //配好路由规则
            config.Routes.MapHttpRoute(
                name: "SmallCodeApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { controller = "Order", id = RouteParameter.Optional }
                );
        }
    }
}